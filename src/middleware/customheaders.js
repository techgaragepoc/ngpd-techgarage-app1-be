/**
 * A middleware to allow custom headers on the response
 */
const customHeaders = (config) => {
  const headers = config.customHeaders();

  return (req, res, next) => {
    if (headers.length > 0) {
      // Enable Custom Headers and additional keys
      res.setHeader('Access-Control-Allow-Headers', headers.join(','));
    }

    // req.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    // req.headers.content = 'application/x-www-form-urlencoded';

    // res.setHeader('apikey', config.apiKey());
    // req.headers['apikey'] = config.apiKey();
    req.headers.apikey = config.apiKey();

    if (req.method === 'OPTIONS') {
      // console.log('options', req.headers);
      res.status(200).end();
    } else {
      next();
    }
  };
};

module.exports = customHeaders;
