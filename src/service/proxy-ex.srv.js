const { Request } = require('ngpd-merceros-common-be/util');
const config = require('../config');

class ProxyExampleService {

  constructor() {
    this.config = config;
    this.request = new Request(config);
  }

  getExampleData(msSlag) {
    const reqMethod = this.request.get.bind(this.request);
    const baseUrl = this.config.api_url_microservice_to_aggregate();
    const slug = `/api/${msSlag}`;
    return reqMethod(`${baseUrl}${slug}`);
  }

}

module.exports = ProxyExampleService;
