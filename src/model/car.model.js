const mongoose = require('ngpd-merceros-common-be/mongoose');

const userSchema = new mongoose.Schema(
  {
    brand: {
      type: String,
      required: true,
    },
    model: {
      type: String,
      required: true,
    },
  }, {
    collection: 'cars',
  }
);

module.exports = mongoose.model('car', userSchema);
