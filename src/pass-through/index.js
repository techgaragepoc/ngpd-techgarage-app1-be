const express = require('express');
const config = require('../config');
const { PassThroughController } = require('ngpd-merceros-common-be/controller');

const router = express.Router();
const psBaseUrl = config.api_url_microservice_pass_through();
const psController = new PassThroughController(config, psBaseUrl);

router.route('/*')
  .get(psController.callMethod('get'))
  .post(psController.callMethod('post'))
  .put(psController.callMethod('put'))
  .delete(psController.callMethod('delete'));

module.exports = router;
