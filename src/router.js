const passThroughRoute = require('./pass-through');
const allRoutes = require('./controller');

module.exports = (rootRouter) => {
  // EXAMPLE OF PASS THROUGH
  rootRouter.use('/api/example', passThroughRoute);

  // EXAMPLE OF OWN CONTROLLER FROM MID TIER
  rootRouter.use('/api', allRoutes);
};
