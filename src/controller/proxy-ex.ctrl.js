const { RestController } = require('ngpd-merceros-common-be/controller');
const { ProxyExampleService } = require('../service');

class ProxyExampleRestController extends RestController {

  constructor() {
    super();
    this.proxyExampleService = new ProxyExampleService();
  }

  getExampleData(req, res, next) {
    const msSlug = req.params[0];

    this.proxyExampleService
      .getExampleData(msSlug)
      .then((exampleData) => {
        const agg = {
          exampleData,
          aggregation: 'aggregation'
        };
        return agg;
      })
      .then(this.responseWithResult(res))
      .catch(next);
  }
}

module.exports = ProxyExampleRestController;
