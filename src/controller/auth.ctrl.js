const { RestController } = require('ngpd-merceros-common-be/controller');
const { CarService } = require('../service');

class AuthRestController extends RestController {
  constructor() {
    super({});
    this.carService = new CarService();
  }

  generateHS256Token(req, res, next) {
    const { body } = req;
    return this.carService.createCar(body)
      .then(this.responseWithResult(res))
      .catch(next);
  }

  generateRS256Token(req, res, next) {
    const { body } = req;
    return this.carService.createCar(body)
      .then(this.responseWithResult(res))
      .catch(next);
  }
}

module.exports = AuthRestController;
