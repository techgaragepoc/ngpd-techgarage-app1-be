const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const { SpecReporter } = require('jasmine-spec-reporter');
const config = require('../config');

const enforceCoverageThreshold = 95; // min. 95% test coverage

gulp.task('prepare-unit-test', () =>
    gulp.src(config.paths.sources)
    // Covergulp-jasmineing files, including those without any tests at all
    .pipe($.istanbul({ includeUntested: true }))
    // Force `require` to return covered files
    .pipe($.istanbul.hookRequire()));

gulp.task('unit-test', ['prepare-unit-test'], () =>
    gulp.src(config.paths.specs)
    // Running tests with jasmine, output each test case
    .pipe($.jasmine({
      verbose: true,
      reporter: new SpecReporter(),
    }))
    // Creating the reports after tests ran
    .pipe($.istanbul.writeReports())
    // Enforce a coverage of at least 95%
    .pipe($.istanbul.enforceThresholds({ thresholds: { global: enforceCoverageThreshold } }))
    .on('error', (error) => {
      if (error.plugin === 'gulp-istanbul' && error.message === 'Coverage failed') {
        return Object.assign({}, error, { message: `Coverage has to be at least ${enforceCoverageThreshold}%!` });
      }
      return error;
    }));

gulp.task('unit-test-without-coverage', () =>
    gulp.src(config.paths.specs)
    // Running tests with jasmine, output each test case
    .pipe($.jasmine({ verbose: true })));
