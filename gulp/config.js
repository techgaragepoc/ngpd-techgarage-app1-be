module.exports = {
  paths: {
    specs: ['gulp/test-setup.spec.js', 'app/**/*.spec.js'],
    integration: ['test-setup.spec.js', 'app/**/*.integration.js'],
    sources: ['app/**/!(*.spec|*.integration).js', '!app/config.js'],
    elasticMocks: ['mocks/fuzzy.json'],
  },
};
