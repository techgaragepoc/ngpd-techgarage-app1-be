const express = require('express');
const {
  MidTierBootstrap,
} = require('ngpd-merceros-common-be/bootstrap');

const appRoutes = require('./src/router');
const config = require('./src/config');
const { customHeaders } = require('./src/middleware');

const expressApp = express();
const expressRouter = express.Router();

const midTierBootstrap = new MidTierBootstrap({
  app: expressApp,
  rootRouter: expressRouter,
  config,
});

midTierBootstrap
  .init()
  .cors()
  // .headers()
  // .db()
  // .auth()
  .inject(({ app, rootRouter }, done) => {
    // app.use(rootRouter, customHeaders(config));
    app.use('/*', customHeaders(config));
    done();
  })
  .inject(({ app, rootRouter }, done) => {
    app.use(rootRouter);
    appRoutes(rootRouter);
    // app.listen(config.port())
    done();
  })
  .error();

module.exports = expressApp;
