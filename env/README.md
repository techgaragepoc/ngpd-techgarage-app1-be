## ENVIRONMENT VARIABLES

In this directory we have env variables. They are used to simplify
deployment on different environments (QA, TEST, PRODUCTION etc.)

They could parsed by shell script on DevOps side, so format is
strict *key=value*

No empty string allowed, no comments allowed. 

Parsing logic 
each time recreates all env vars from this files.

For Now only **QA** file is parsing to real values. But please
don't forget to update other files when you need add/update 
variable. 

File name  contains from:

- *.env.* - fixed prefix
- *<env_name>* - name of environment on devops side 
- *.properties* - fixed suffix 

The env files:

- .env.dev.properties - local development
- .env.prod.properties - production environment
- .env.qa.properties - QA environment
- .env.test.properties - locally test-running environment


### Variables Description

###### Application name
* APP_NAME

###### Application environment
* APP_ENV

###### Node environment
* NODE_ENV

###### Port number
* PORT

###### Show debug messages 

https://www.npmjs.com/package/debug#environment-variables 
* DEBUG=*

###### Frontend URL for CORS

* FRONTEND_URL

###### URL path after successful login

* FRONTEND_PATH_AFTER_LOGIN

###### URL path after failed login

* FRONTEND_PATH_AFTER_FAILURE

###### Request timeout

* REQUEST_TIMEOUT

###### Redirects count

* REQUEST_FOLLOW

###### SSO entry point

* SSO_ENTRY_POINT

###### SSO private certificate

* SSO_PRIVATE_CERT

###### Backend URL

* BACKEND_URL

###### Session secret

* SESSION_SECRET

###### Session cookie name

* SESSION_COOKIE_NAME

###### MongoDB collection for sessions

* SESSION_COLLECTION

###### URI to MongoDB

* MONGODB_URI
e.g.: mongodb://localhost/app-name-change-me

###### HTTP header names which must not get logged for security reasons
comma-separated string, e.g. header1,header2
* SECURED_HEADERS

###### Cookie names which must not get logged for security reasons
comma-separated string, e.g. cookie1,cookie2
* SECURED_COOKIES

###### Microservice URL for your endpoint

* API_MICROSERVICE_PASS_THROUGH
